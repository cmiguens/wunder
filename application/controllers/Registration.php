<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration extends CI_Controller {

	public function postData()
	{
		error_reporting(E_ALL);
		header("Content-Type: text/html");
		// retrieve information
		$form_data = $this->input->post();
		if (!isset($form_data['firstname']))
		{
			$this->index();
			return;
		}
		// insert data in the database
		$this->load->model('user_model');
		$this->user_model->insert_entry($form_data);
		// push data to demo payment API
		$data = '{"customerId":'.$this->user_model->id.',"iban":"'.$this->user_model->IBAN.'", "owner":"'.$this->user_model->accountowner.'"}';

		//open connection
		$url = 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data';
		$ch = curl_init($url);
		//set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POST,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 20); //timeout in seconds
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		//execute post
		$result = curl_exec($ch);
	    $error = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		//close connection
		curl_close($ch);
		$tittle = "Sussesful";
		$message = "Payment data id: ";
		if ((isset($error)) && ($error != 200))
		{
			$tittle = "The user can't be created";
			$errorreply = json_decode($result, true);
			if (isset($errorreply["message"]))
				$message = "Error pushing the data to the demo payment API <br>". $errorreply["message"];
			else
				$message = "Error pushing the data to the demo payment API <br>".$result;
			$message = $error;
		} else {
			$response = json_decode($result, true);
			if (isset($response["paymentDataId"]))
			{
				$message = $message . $response["paymentDataId"];	
				$this->user_model->update_entry($response["paymentDataId"]);
			}
			else
				$message = "Error reading the response of the demo payment API <br>".$result;
		}

	    $reply["tittle"] = $tittle;
	    $reply["message"] = $message;
		$this->load->view('user_confirmation', $reply);	
	}

	public function index()
	{
		$this->load->view('user_registration');
	}
}
