<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Wunder + Carlos Miguens</title>

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#data-user {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>

<div id="container">
	<h1>Welcome to user registration!</h1>
	<form id ="userinput" action="<?php echo base_url(); ?>index.php/Registration/postData" method="post">

	<div id="regform" style="margin: 0 15px 0 15px;"></div>

	<div id="data-user" style="visibility: hidden;position: absolute;">
		<label>Step 1: personal information</label><br>
		<br>
	    <label for="firstname"><b>First name</b></label>
	    <input type="text" placeholder="Enter your firstname" id="firstname" name="firstname" onblur="javascript:saveCookies(this);" required> 
	    <br>

	    <label for="lastname"><b>Last name</b></label>
	    <input type="text" placeholder="Enter your lastname" id="lastname" name="lastname" onblur="javascript:saveCookies(this);" required> 
	    <br>

	    <label for="phone"><b>Phone</b></label>
	    <input type="text" placeholder="Enter your phone number" id="phone" name="phone" onblur="javascript:saveCookies(this);"> 
	    <br>

	    <br> 

	    <input type="button" value = "Next" onclick="javascript:nextStep();">
	</div>

	<div id="data-address" style="visibility: hidden;position: absolute;">
		<label>Step 2: address information</label><br>
		<br>
	    <label for="street"><b>Street</b></label>
	    <input type="text" placeholder="Enter your street address" id="street" name="street" onblur="javascript:saveCookies(this);" required> 
	    <br>

	    <label for="number"><b>Number</b></label>
	    <input type="text" placeholder="House number" id="number" name="number" onblur="javascript:saveCookies(this);" required> 
	    <br>

	    <label for="zipcode"><b>Zipcode</b></label>
	    <input type="text" placeholder="Zipcode" id="zipcode" name="zipcode" onblur="javascript:saveCookies(this);"> 
	    <br>

	    <label for="city"><b>City</b></label>
	    <input type="text" placeholder="City" id="city" name="city" onblur="javascript:saveCookies(this);"> 
	    <br>

	    <br> 
	    <input type="button" value = "Previous" onclick="javascript:prevStep();">
	    <input type="button" value = "Next" onclick="javascript:nextStep();">
	</div>

	<div id="data-payment" style="visibility: hidden;position: absolute;">
		<label>Step 3: payment information</label><br>
		<br>
	    <label for="accountowner"><b>Account owner</b></label>
	    <input type="text" placeholder="Account owner" id="accountowner" name="accountowner" onblur="javascript:saveCookies(this);" required> 
	    <br>

	    <label for="iban"><b>IBAN</b></label>
	    <input type="text" placeholder="iban" id="iban" name="iban" onblur="javascript:saveCookies(this);" required> 

	    <br> 
	    <input type="button" value = "Previous" onclick="javascript:prevStep();">
	    <input type="button" class="btn btn-success" onclick="javascript:nextStep();" value = "Submit">
	</div>

	</form>
	<p class="footer">Carlos Miguens <a href = "https://www.linkedin.com/in/cmiguens/">LinkedIn</a> <a href = "https://github.com/devtodev">Github</a></p>
</div>

	<script language="javascript">
		var step = 1;

		function showStep()
		{
			if (step == 1)
				document.getElementById("regform").innerHTML = document.getElementById("data-user").innerHTML;
			if (step == 2)	
				document.getElementById("regform").innerHTML = document.getElementById("data-address").innerHTML;
			if (step == 3)	
				document.getElementById("regform").innerHTML = document.getElementById("data-payment").innerHTML;

			if (step == 4)
				document.getElementById("regform").innerHTML = "sending..";
			
			if ((document.cookie != null) && (document.cookie != ""))
			{
				var cookies = JSON.parse(getCookie("userdata"));
				if (cookies.firstname != null)
					document.getElementById("firstname").value = cookies.firstname;
				if (cookies.lastname != null)
					document.getElementById("lastname").value = cookies.lastname;
				if (cookies.phone != null)
					document.getElementById("phone").value = cookies.phone;
				if (cookies.street != null)
					document.getElementById("street").value = cookies.street;
				if (cookies.number != null)
					document.getElementById("number").value = cookies.number;
				if (cookies.zipcode != null)
					document.getElementById("zipcode").value = cookies.zipcode;
				if (cookies.city != null)
					document.getElementById("city").value = cookies.city;
				if (cookies.accountowner != null)
					document.getElementById("accountowner").value = cookies.accountowner;
				if (cookies.iban != null)
					document.getElementById("iban").value = cookies.iban;
			}

			if (step == 4)
			{
				document.getElementById("regform").innerHTML = "sending..";
				document.getElementById("userinput").submit();
			} else {
				cookies.step = step;
				document.cookie = "userdata="+JSON.stringify(cookies) + ";max-age=600";
			}
		}

		function checkValue(component)
		{
			if (component.value == "")
			{
				alert("Fill " + component.id);
				component.focus();
				return true;
			}
			return false;
		}

		function nextStep() {
			if (step == 1)
			{
				if (checkValue(document.getElementById("firstname"))) return;
				if (checkValue(document.getElementById("lastname"))) return;
			}
			if (step == 2)
			{
				if (checkValue(document.getElementById("street"))) return;
				if (checkValue(document.getElementById("number"))) return;
				if (checkValue(document.getElementById("zipcode"))) return;
				if (checkValue(document.getElementById("city"))) return;
			}
			if (step == 3)
			{
				if (checkValue(document.getElementById("accountowner"))) return;
				if (checkValue(document.getElementById("iban"))) return;
			}
			step++;
			showStep();
		}

		function prevStep() {
			step--;
			showStep();
		}

		function getCookie(cname) {
		    var name = cname + "=";
		    var decodedCookie = decodeURIComponent(document.cookie);
		    var ca = decodedCookie.split(';');
		    for(var i = 0; i <ca.length; i++) {
		        var c = ca[i];
		        while (c.charAt(0) == ' ') {
		            c = c.substring(1);
		        }
		        if (c.indexOf(name) == 0) {
		            return c.substring(name.length, c.length);
		        }
		    }
		    return "";
		}

		function saveCookies(component) {
			var cookie = {};
			if ((document.cookie != null) && (document.cookie != ""))
			{
				cookie = JSON.parse(getCookie("userdata"));
			}
			cookie[component.id] = component.value;
			cookie['step'] = step;
			document.cookie = "userdata="+JSON.stringify(cookie) + ";max-age=600";
		}

		function startUp() {
			step = 1;
			if ((document.cookie != null) && (document.cookie != ""))
			{
				try {
					var cookies = JSON.parse(getCookie("userdata"));
					step = cookies.step;
				} catch(e) {
					document.cookie = "userdata=;max-age=0"; // this is to clean the cookies, don't chage it
				}
			}
			showStep(step);
		}
		startUp();
	</script>

</body>
</html>