<?php 

class User_model extends CI_Model {

        public $id;
        public $firstname;
        public $lastname;
        public $telephone;
        public $street;
        public $number;
        public $zipcode;
        public $city;
        public $IBAN;
        public $accountowner;
        public $paymentdataid;

        public function insert_entry($data)
        {
                $this->firstname = $data['firstname'];
                $this->lastname = $data['lastname'];
                $this->telephone = $data['phone'];
                $this->street = $data['street'];
                $this->number = $data['number'];
                $this->zipcode = $data['zipcode'];
                $this->city = $data['city'];
                $this->IBAN = $data['iban'];
                $this->accountowner = $data['accountowner'];
                $this->db->insert('users', $this);
                $this->id = $this->db->insert_id();
        }

        public function update_entry($PDI)
        {
                $this->paymentdataid = $PDI; 
                $this->db->update('users', $this, array('id' => $this->id));
        }

}