# Wunder senior PHP hiring 

Carlos Miguens application, for more information please write me to cmiguens@gmail.com

## Getting Started

These project was made with PHP/MySQL usign MVC pattern design implementing Codeigniter framework .

### Prerequisites

The following libraries are neccessary to run this web app.

```
Curl, MySQL (5.1+), Apache or another web server with PHP version 5.6 or newer is recommended.
```

### Installing

Ensure you have all the requirements installed in your server.

To connect to database please setup the hostname, username, password and database name in the following file:

```
/wunder/application/config/database.php
```

To create the users table run this script, please remember change the name of the database:

```
CREATE TABLE `wundertest`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `firstname` VARCHAR(60) NOT NULL,
  `lastname` VARCHAR(60) NOT NULL,
  `telephone` VARCHAR(45) NULL,
  `street` VARCHAR(60) NOT NULL,
  `number` VARCHAR(45) NOT NULL,
  `zipcode` VARCHAR(45) NOT NULL,
  `city` VARCHAR(45) NOT NULL,
  `accountowner` VARCHAR(60) NOT NULL,
  `IBAN` VARCHAR(45) NOT NULL,
  `paymentdataid` VARCHAR(150) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC));

```

* A dump of the database you will find it at schema.sql

## Considerations

* If you install this web app into a RedHat, CentOS or Fedora server with SELinux enabled run the following command line to enable the curl response.

```
# setsebool -P httpd_can_network_connect 1
```

* Before submit the data into the database, the entries are stored in cookies with 10 minutes of lifetime. You can change it in user_registration.php file lines 249 and 183. 

### Performance Optimizations

* In this case codeigniter was chosen, but to improve the code size I could be used another smaller framework like Slim.

### Contact information

* Name: Carlos Miguens
* email: cmiguens@gmail.com